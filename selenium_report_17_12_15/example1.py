#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# 1. Создаем объект webdriver
driver = webdriver.Firefox()
# 2. Открываем веб-страницу
driver.get("http://ya.ru")
# 3. Устанавливаем величину задержки
driver.implicitly_wait(30)
# 4. Ищем элементы 
inputField = driver.find_element_by_name("text")
# 5. Выполняем действия над элементами
inputField.send_keys("Selenium")
inputField.send_keys(Keys.RETURN)
# Закрываем браузер
driver.close()
