#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from unittest import TestCase, main 

# Наследуемся от TestCase
class ExampleSeleniumTest(TestCase):
    def testSearch(self):
        driver = webdriver.Firefox()
        driver.get("http://ya.ru")
        driver.implicitly_wait(30)
        inputField = driver.find_element_by_name("text")
        inputField.send_keys("Selenium")
        inputField.send_keys(Keys.RETURN)
        # 6. Ищем элемент для проверки (по названию тега)
        body = driver.find_element_by_tag_name("body1")
        # 7. Проверяем 
        self.assertIsNotNone(body)
        # Закрываем браузер
        driver.close()

if __name__ == '__main__':
    main()
