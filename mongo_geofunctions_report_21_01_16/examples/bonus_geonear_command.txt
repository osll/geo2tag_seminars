db.runCommand(
    {
      geoNear: "test_collection1", # Коллекция
      near: {
            type: "Point", 
            coordinates: [ 0, 1 ] }, # Точка отсчета
      spherical: true,
      query: {name : '1'}, # Дополнительное ограничение на фильтруемые документы
      maxDistance: 9999999 # Метры
    } )
