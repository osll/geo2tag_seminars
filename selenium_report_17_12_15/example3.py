#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from unittest import TestCase, main 

class ExampleJsSeleniumTest(TestCase):
    def testQunitResults(self):
        driver = webdriver.Firefox()
        # Подставьте в кавычки свой путь к файлу example3.html 
        driver.get("file:///home/vood/ws/geo2tag_seminars/selenium_report_17_12_15/example3.html")
        # Ждем когда тесты завершаться  
        WebDriverWait(driver, 10).until(
            expected_conditions.text_to_be_present_in_element(
                (By.ID, "qunit-testresult"), 'Tests completed in')
        )
        # Проверяем, что нет упавших тестов - должен быть такой элемент:
        # <span class="failed">0</span>
        failed_number_element = driver.find_element_by_class_name("failed")
        failed_number = failed_number_element.get_attribute("innerHTML")
        self.assertEquals(int(failed_number), 0)
        # Закрываем браузер
        driver.close()

if __name__ == '__main__':
    main()
